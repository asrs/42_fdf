/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 07:48:01 by clrichar          #+#    #+#             */
/*   Updated: 2018/02/24 14:47:21 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <stdlib.h>
# include <unistd.h>
# include <math.h>
# include "mlx.h"
# include "libft.h"

# define WIN1_W 1920
# define WIN1_H 1080

# define WIN data->g_env
# define COORD data->coord
# define IMG data->img
# define WHITE 0xFFFFFF
# define GREEN 0x00FF00
# define RED 0xFF0000
# define YELLOW 0xFFFF00
# define BLUE 0x0000FF
# define PURPLE 0x710856
# define CYAN 0x1DE8C6

typedef struct		s_env
{
	void			*mlx_ptr;
	void			*win_ptr;
	void			*img_ptr;
	int				win_w;
	int				win_h;
	int				xgap;
	int				ygap;
	double			off_x;
	double			off_y;
}					t_env;

typedef struct		s_img
{
	char			*addr;
	int				bpp;
	int				sizeline;
	int				endian;
}					t_img;

typedef struct		s_point
{
	double			x;
	double			y;
	double			z;
	double			sx;
	double			sy;
	double			sz;
	int				act;
	int				colors;
}					t_point;

typedef struct		s_data
{
	t_env			g_env;
	t_img			img;
	t_point			*coord;
	size_t			mapx;
	size_t			mapy;
	size_t			size;
	int				pix;
	int				piy;
	int				margin;
	int				high;
	int				act;
	int				reset_margin;
	double			angle_x;
	double			angle_y;
	double			angle_z;
	int				base_colors[7];
	int				colors_index;
}					t_data;

void				m_indication(void);
void				m_parse(char *map, t_data *data);
void				m_print(t_data *data);
void				m_rotation(t_data *data, size_t y, size_t x);
void				m_bresenham(t_data *data, size_t act_pos, size_t old_pos);
int					m_hook(int key, t_data *data);
int					m_countnumber(const char *line);
void				m_change_color(t_data *data, int new_color);
void				m_reset(t_data *data);
void				m_quit(t_data *data);
void				mlx_pixel_to_img(t_data *data, int x, int y, int color);
void				mlx_redraw_img(t_data *data);
void				m_error(const char *message, t_data *data);

#endif
