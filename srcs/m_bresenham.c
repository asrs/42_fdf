/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_bresenham.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 16:38:00 by clrichar          #+#    #+#             */
/*   Updated: 2018/02/22 16:38:12 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void			ft_trace_h(t_data *data, double *distance, \

		double *inc, double *source)
{
	double			i;
	double			cumul;

	i = 0;
	cumul = distance[0] / 2;
	while (i < distance[0])
	{
		source[0] += inc[0];
		cumul += distance[1];
		if (cumul >= distance[0])
		{
			cumul -= distance[0];
			source[1] += inc[1];
		}
		mlx_pixel_to_img(data, (int)source[0], (int)source[1], (int)source[2]);
		i++;
	}
}

static void			ft_trace_v(t_data *data, double *distance, \
		double *inc, double *source)
{
	double			i;
	double			cumul;

	i = 0;
	cumul = distance[1] / 2;
	while (i < distance[1])
	{
		source[1] += inc[1];
		cumul += distance[0];
		if (cumul >= distance[1])
		{
			cumul -= distance[1];
			source[0] += inc[0];
		}
		mlx_pixel_to_img(data, (int)source[0], (int)source[1], (int)source[2]);
		i++;
	}
}

void				m_bresenham(t_data *data, size_t start, size_t end)
{
	double			distance[2];
	double			inc[2];
	double			source[3];

	source[0] = COORD[start].x;
	source[1] = COORD[start].y;
	source[2] = COORD[start].colors;
	distance[0] = COORD[end].x - source[0];
	distance[1] = COORD[end].y - source[1];
	inc[0] = (distance[0] > 0) ? 1 : -1;
	inc[1] = (distance[1] > 0) ? 1 : -1;
	distance[0] = (distance[0] < 0) ? distance[0] * -1 : distance[0];
	distance[1] = (distance[1] < 0) ? distance[1] * -1 : distance[1];
	mlx_pixel_to_img(data, (int)source[0], (int)source[1], PURPLE);
	if (distance[0] > distance[1])
		ft_trace_h(data, distance, inc, source);
	else
		ft_trace_v(data, distance, inc, source);
}
