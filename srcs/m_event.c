/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_event.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 16:38:01 by clrichar          #+#    #+#             */
/*   Updated: 2018/02/24 14:51:44 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void				m_indication(void)
{
	ft_putendl("Bienvenu  :: use ARROWS to move the wireframe.");
	ft_putendl("--------  :: use 'A' 'S' 'D', to change angle.");
	ft_putendl("--------  :: use 'C', to change the color");
	ft_putendl("--------  :: use '0', to reset");
	ft_putendl("--------  :: use '-' '=', to zoom and dezoom");
	ft_putendl("--------  :: use '[' ']', to increase or decrease high");
	ft_putendl("Thank you :: Please use 'escape' for leaving the programs");
}

void				m_change_color(t_data *data, int new_color)
{
	size_t			y;
	size_t			x;
	size_t			pos;

	y = 0;
	x = 0;
	new_color = (new_color > 6) ? 0 : new_color;
	while (y < data->mapy)
	{
		x = 0;
		while (x < data->mapx)
		{
			pos = (y * data->mapx) + x;
			if (COORD[pos].colors == data->base_colors[data->colors_index])
				COORD[pos].colors = data->base_colors[new_color];
			x++;
		}
		y++;
	}
	data->colors_index = new_color;
}

void				m_reset(t_data *data)
{
	data->angle_x = 120;
	data->angle_y = 120;
	data->angle_z = 120;
	data->pix = 0;
	data->piy = 0;
	data->margin = data->reset_margin;
	m_change_color(data, 0);
	mlx_redraw_img(data);
}

void				m_error(const char *message, t_data *data)
{
	ft_putendl_fd(message, 2);
	if (data && data->coord)
	{
		free(data->coord);
		data->coord = NULL;
	}
	exit(EXIT_FAILURE);
}

void				m_quit(t_data *data)
{
	if (WIN.img_ptr)
		mlx_destroy_image(WIN.mlx_ptr, WIN.img_ptr);
	if (WIN.win_ptr)
		mlx_destroy_window(WIN.mlx_ptr, WIN.win_ptr);
	if (data->coord)
	{
		free(data->coord);
		data->coord = NULL;
	}
	exit(EXIT_SUCCESS);
}
