/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 16:38:03 by clrichar          #+#    #+#             */
/*   Updated: 2018/02/24 14:47:28 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int				m_countnumber(const char *line)
{
	int			i;

	i = 0;
	while (*line)
	{
		if (ft_isdigit(*line) == 1)
			i++;
		line++;
	}
	return (i);
}

void			mlx_pixel_to_img(t_data *data, int x, int y, int color)
{
	int			color1;
	int			color2;
	int			color3;
	int			bit_pix;
	float		img_size;

	img_size = (float)(WIN1_W * WIN1_H * IMG.bpp / 8);
	if (x < 0 || y < 0 || y * IMG.sizeline + x * \
			IMG.bpp / 8 > img_size || x >= IMG.sizeline /\
			(IMG.bpp / 8) || y >= (int)img_size / IMG.sizeline)
		return ;
	color1 = color;
	color2 = color >> 8;
	color3 = color >> 16;
	bit_pix = IMG.bpp / 8;
	IMG.addr[y * IMG.sizeline + x * bit_pix] = (char)color1;
	IMG.addr[y * IMG.sizeline + x * bit_pix + 1] = (char)color2;
	IMG.addr[y * IMG.sizeline + x * bit_pix + 2] = (char)color3;
}

void			mlx_redraw_img(t_data *data)
{
	mlx_destroy_image(WIN.mlx_ptr, WIN.img_ptr);
	WIN.img_ptr = NULL;
	m_print(data);
}
