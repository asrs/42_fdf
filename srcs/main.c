/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 16:38:02 by clrichar          #+#    #+#             */
/*   Updated: 2018/02/24 14:51:45 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

#define WIN_W data->g_env.win_w
#define WIN_H data->g_env.win_h

static void				ft_init_win(t_data *data)
{
	WIN_W = WIN1_W;
	WIN_H = WIN1_H;
	WIN.xgap = 0;
	WIN.ygap = 0;
	WIN.off_x = 0;
	WIN.off_y = 0;
	WIN.mlx_ptr = mlx_init();
	if (!WIN.mlx_ptr)
		exit(1);
	WIN.win_ptr = mlx_new_window(WIN.mlx_ptr, WIN_W, WIN_H, (char *)"FDF");
}

static void				ft_init_struct(t_data *data)
{
	data->mapx = 0;
	data->mapy = 0;
	data->size = 0;
	data->margin = 0;
	data->reset_margin = 0;
	data->high = 0;
	data->pix = 0;
	data->piy = 0;
	data->act = 0;
	data->coord = NULL;
	data->angle_x = 120;
	data->angle_y = 120;
	data->angle_z = 120;
	data->base_colors[0] = WHITE;
	data->base_colors[1] = RED;
	data->base_colors[2] = GREEN;
	data->base_colors[3] = YELLOW;
	data->base_colors[4] = BLUE;
	data->base_colors[5] = PURPLE;
	data->base_colors[6] = CYAN;
	data->colors_index = 0;
	IMG.addr = NULL;
}

static void				ft_fdf(char *map)
{
	t_data				data;

	ft_init_struct(&data);
	m_parse(map, &data);
	m_indication();
	ft_init_win(&data);
	m_print(&data);
	mlx_hook(data.g_env.win_ptr, 2, (1L << 0), m_hook, &data);
	mlx_key_hook(data.g_env.win_ptr, m_hook, &data);
	mlx_loop(data.g_env.mlx_ptr);
}

int						main(int ac, char **av)
{
	if (ac != 2)
	{
		ft_putendl("Usage : ./fdf input_map");
		return (-1);
	}
	else if (ac == 2 && ft_strequ("", av[1]))
	{
		m_error("Bad Map - Failure", NULL);
		return (-1);
	}
	else
		ft_fdf(av[1]);
	return (0);
}
