/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_rotation.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 16:38:02 by clrichar          #+#    #+#             */
/*   Updated: 2018/02/24 13:51:09 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		ft_axe_x(t_data *data, size_t pos)
{
	double		sinus;
	double		cosinus;
	double		tmpy;
	double		tmpz;

	sinus = sin(data->angle_x);
	cosinus = cos(data->angle_x);
	tmpy = COORD[pos].y;
	tmpz = COORD[pos].z;
	COORD[pos].y = (tmpy * cosinus) + (tmpz * -sinus);
	COORD[pos].z = (tmpy * (sinus)) + (tmpz * cosinus);
}

static void		ft_axe_y(t_data *data, size_t pos)
{
	double		sinus;
	double		cosinus;
	double		tmpx;
	double		tmpz;

	sinus = sin(data->angle_y);
	cosinus = cos(data->angle_y);
	tmpx = COORD[pos].x;
	tmpz = COORD[pos].z;
	COORD[pos].x = (tmpx * cosinus) + (tmpz * -sinus);
	COORD[pos].z = (tmpx * (sinus)) + (tmpz * cosinus);
}

static void		ft_axe_z(t_data *data, size_t pos)
{
	double		sinus;
	double		cosinus;
	double		tmpx;
	double		tmpy;

	sinus = sin(data->angle_z);
	cosinus = cos(data->angle_z);
	tmpx = COORD[pos].x;
	tmpy = COORD[pos].y;
	COORD[pos].x = (tmpx * cosinus) + (tmpy * -sinus);
	COORD[pos].y = (tmpx * (sinus)) + (tmpy * cosinus);
}

void			m_rotation(t_data *data, size_t y, size_t x)
{
	size_t		pos;

	pos = (y * data->mapx) + x;
	ft_axe_x(data, pos);
	ft_axe_y(data, pos);
	ft_axe_z(data, pos);
}
