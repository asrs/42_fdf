/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_print.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 15:17:01 by clrichar          #+#    #+#             */
/*   Updated: 2018/02/24 15:17:03 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

#define WIN_W WIN1_W
#define WIN_H WIN1_H

static void		ft_padding(t_data *data)
{
	int			tmp;

	tmp = 0;
	WIN.xgap = (int)((WIN_W / 2) / (int)(data->mapx + 1));
	WIN.ygap = (int)((WIN_H / 2) / (int)(data->mapy + 1));
	tmp = (WIN.xgap < WIN.ygap) ? WIN.xgap : WIN.ygap;
	data->margin += tmp;
	data->reset_margin += tmp;
	tmp = (data->margin * 3);
	data->high += tmp;
	WIN.xgap = (int)((WIN_W - ((int)(data->mapx) * data->margin)) / 2);
	WIN.ygap = (int)((WIN_H - ((int)(data->mapy) * data->margin)) / 2);
}

static void		ft_transform(t_data *data, size_t y, size_t x)
{
	size_t		pos;

	pos = 0;
	while (y < data->mapy)
	{
		x = 0;
		while (x < data->mapx)
		{
			pos = (y * data->mapx) + x;
			COORD[pos].x = COORD[pos].sx + (data->margin * (int)x) + data->pix;
			COORD[pos].y = COORD[pos].sy + (data->margin * (int)y) + data->piy;
			COORD[pos].z = COORD[pos].sz + data->high;
			m_rotation(data, y, x);
			x++;
		}
		y++;
	}
}

static void		ft_center(t_data *data, size_t y, size_t x)
{
	size_t		pos;

	pos = 0;
	WIN.off_x = COORD[data->mapx / 2].x - COORD[0].x;
	WIN.off_y = COORD[data->mapy / 2].y - COORD[0].y;
	WIN.off_x = ((WIN_W - WIN.off_x) / 2);
	WIN.off_y = ((WIN_H - WIN.off_y) / 3);
	while (y < data->mapy)
	{
		x = 0;
		while (x < data->mapx)
		{
			pos = (y * data->mapx) + x;
			COORD[pos].x += WIN.off_x;
			COORD[pos].y += WIN.off_y;
			x++;
		}
		y++;
	}
}

static void		ft_trace(t_data *data)
{
	size_t		y;
	size_t		x;
	size_t		pos;
	size_t		prev;

	y = 0;
	x = 0;
	pos = 0;
	prev = 0;
	while (y < data->mapy)
	{
		x = 0;
		while (x < data->mapx)
		{
			pos = (y * data->mapx) + x;
			prev = (x > 0) ? (pos - 1) : pos;
			m_bresenham(data, prev, pos);
			prev = (y > 0) ? (((y - 1) * data->mapx) + x) : pos;
			m_bresenham(data, prev, pos);
			x++;
		}
		y++;
	}
}

void			m_print(t_data *data)
{
	if (data->act == 0)
	{
		data->act = 1;
		ft_padding(data);
	}
	WIN.img_ptr = mlx_new_image(WIN.mlx_ptr, (int)WIN_W, (int)WIN_H);
	IMG.addr = mlx_get_data_addr(WIN.img_ptr, &IMG.bpp, \
			&IMG.sizeline, &IMG.endian);
	ft_transform(data, 0, 0);
	ft_center(data, 0, 0);
	ft_trace(data);
	mlx_put_image_to_window(WIN.mlx_ptr, WIN.win_ptr, WIN.img_ptr, 0, 0);
}
